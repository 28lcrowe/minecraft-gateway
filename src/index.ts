import { createServer } from 'net'
import { Connection } from './connection'
import PlayerManager from './player/PlayerManager'
import ServerManager from './server/ServerManager'

const intervalFetchServers = Number(process.env.INTERVAL_FETCH_SERVERS) || 5000
const intervalSendPlayers = Number(process.env.INTERVAL_SEND_PLAYERS) || 3000

ServerManager.updateAvailableServers()
setInterval(() => {
  ServerManager.updateAvailableServers()
  console.log('Interval handle: update servers list')
}, intervalFetchServers)

setInterval(() => {
  PlayerManager.sendCurrentPlayersConnected()
  console.log('Interval handle: send players to servers-service')
}, intervalSendPlayers)


const proxy = createServer()
proxy.on('connection', (socketToClient) => {
  console.log(
    `New incoming connection from: ${socketToClient.remoteAddress}`
  )
  new Connection(socketToClient)
})

proxy.listen('25566', () => {
  console.log('opened server on', proxy.address())
})
